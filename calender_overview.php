<?php
/*
 * Overview multiple Calendar (ical), for full-day-events only
 *
 * https://gitlab.com/soerenj/overview_multiple_ical_calendar_bookings
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation.  Please see LICENSE.txt at the top level of
 * the source code distribution for details.<
 */



$configfile = 'calender_overview_config.txt';
$html_head_add = '';//<link rel="shortcut icon" href="calender_overview_favicon.png" />';
$headlineVerticalFixed = true;

$configurationForm = 'POST'; //choose GET or POST     ;;;
//(best for single User-Mode:) POST will be saved in the permanent Configuration file
//(best for multiple User-Mode:) GET is good if you want to share this webservice, and other can bookmark/save there configuration via URL
$allow_upload_form = false; //choose  true or false   ;;; allow user to upload local ics/ical files
$noindex=true; //searchEngines shall not index this iste;

//global
$event_title_hideNotes_raw = array('Not available','belegt','buchungen','reserved','geblockt');
$event_title_hideNotes = array_map('strtolower',$event_title_hideNotes_raw);
//house Symbol (old utf8: &#8962; ⌂); (newer utf8: &#127968; 🏠)



$showFiveYearsInFuturePreset = false; //can be changed by User (in POST-Forms only temporary)
$showFiveYearsInFuture = $showFiveYearsInFuturePreset;
if( (isset($_GET['showFiveYearsInFuture']) && $_GET['showFiveYearsInFuture']==1))   $showFiveYearsInFuture = true;
if( (isset($_POST['showFiveYearsInFuture']) && $_POST['showFiveYearsInFuture']==1)) $showFiveYearsInFuture = true;



$lang=get__my_locale();
$translated = array();
$translated['de_DE'] = array(
  "Add new entry"=>"Eintrag hinzufügen",
  "Calendar"=>"Kalendar",
  "Configuration"=>"Einstellungen",
  "Customer of" => "Kunde von",
  "Insert here your calendar Export-Adress/URL." => "Hier kommt die Kalendar Export-Adresse/URL rein.",
  "e.g."=>"z.B.",
  "active"=>"aktiv",
  "more settings (unimportant)"=>"weitere Einstellungen (unwichtig)",
  "Show more than 2Years?: 5 Years: " => "Zeige mehr als 2Jahre?: 5Jahre",
  "config: show/hide"=>"Einstellungen zeigen/verstecken",
  "month"=>"Monate",
  "hide"=>"verstecke",
  "Save this Internet-Adress as favorit / bookmark (the long URL with all the parameter) .<br>Than all the configuration-fields are already filled in again, if you visit the bookmark again." => "Speichere diese Internet-Adresse als Favorit / Lesezeichen (die lange Adresse mit den ganzen Parametern). <br> Dann braucht man nicht alle Felder wieder neu ausfüllen.",
  "replace the URL, with example calendar?"=>"Ersetze die Adresse/URL mit Beispiel Kalendar?",
  "add example test file.ics" => "einen Beispiel Kalender.ics einfügen",
  "...to delete an entry: just delete/empty the text in <i>name</i> and <i>url</i>" => "...zum löschen eines Eintrages: einfach Text in <i>Name</i> und <i>url</i> löschen/leer lassen.",
  "Background-Color"=>"Hintergrund-Farbe",
  "color" => "Farbe",
  "Single character Headline" => "Einzelnes Zeichen als Überschrift",
  "a few characters Headline" => "ein paar Zeichen als Überschrift",
  "Cell character" => "Zeichen in jeder Zelle",
  "border width (right)" => "Rand breite (rechts)",
  "First Name part hidden: depending on your settings: possible the first part of the name (all before \" \" or \"_\") will be hidden (sometimes)." => "Erste Namen Teile versteckt/verschluckt: Hängt von den Grund-Einstellungen ab. Möglicherweise wird der erste Teil vom Namen (alles vor \" \" oder \"_\") versteckt (manchmal)",
  "already inserted" => "Bereits eingefügt"
);


$calendar   = array();
$title_calendar = array();
$timetables = array(); 
$seperaLine = array();
$headlinFix = array();
$colors     = array();
$disabled   = array();
$char_cell  = array();
$bg_colors     = array();
$headline_single_char  = array();
$headline_single_char2 = array();
$headline_single_char3 = array();
$headline_single_char4 = array();
$SeperatroValue = ' Seperaturhsa654by ';
$icons     = array();


$configfileTextGET = '';

if($configurationForm=='GET')  $a = $_GET;
if($configurationForm=='POST') $a = $_POST;

  
  
$allow_open_uploaded_files = false;  
if( $allow_upload_form && isset($_FILES) && count($_FILES)>0){
  //var_dump($_FILES);
  //die();
  //$_FILES["files"]["uploaded_files"][0];
  $allow_open_uploaded_files = true;

  for($i=0;$i<count($_FILES["uploaded_files"]['tmp_name']);$i++){
    $n = str_replace($SeperatroValue,'',$_FILES["uploaded_files"]['name'][$i]);
    if(($i+1)%5==0)$border=1; else $border=0;
    $configfileTextGET .= $n.''.$SeperatroValue.$SeperatroValue.''.$_FILES["uploaded_files"]['tmp_name'][$i].$SeperatroValue.$border.$SeperatroValue.'#000088'.$SeperatroValue.$SeperatroValue.($i+1).$SeperatroValue.'#c4c4f6'.$SeperatroValue.$SeperatroValue.$SeperatroValue.$SeperatroValue.$SeperatroValue.$SeperatroValue.$SeperatroValue."\n";
  }
}elseif($allow_upload_form){
  if( isset($_GET['uploaded_files_form'])){
  echo "<h2>Upload Kalendar-Files</h2>Upload .ics/.ical - Files from you local Computer.<br>Cant be mixed with online iCal urls (online files).<br><br>You can select multiple files (via pressing STRG on Keyboard, while selecting files)<br><form method=\"POST\" enctype=\"multipart/form-data\"><input type=\"file\" name=\"uploaded_files[]\" multiple /><br><!--Select mutliple with STRG (pressed on Keyboard)--><br><input type=\"submit\" /></form>";die();
  }
}

//prepare & write config
if( isset($a['name'])){
  $lines='';
  foreach( $a['url'] as $i => $url){
      if( trim($a['name'][$i])=='' && trim($url)=='')continue;
      if($a['name'][$i]=='') $a['name'][$i] = 'rand'.rand(5,216);
      $a['name'] = str_replace($SeperatroValue,'',$a['name']);
      $a['headline'] = str_replace($SeperatroValue,'',$a['headline']);
      $a['url'] = str_replace($SeperatroValue,'',$a['url']);
	  $a['inserat_link'] = str_replace($SeperatroValue,'',$a['inserat_link']);
	  $a['inserat_login'] = str_replace($SeperatroValue,'',$a['inserat_login']);
      $values = array( $a['name'][$i], $a['headline'][$i], $a['url'][$i], $a['seperaLine'][$i], $a['color'][$i], $a['disable'][$i], $a['char_cell'][$i], $a['bg_color'][$i], $a['headline_single_char'][$i],$a['headline_single_char2'][$i], $a['inserat_link'][$i], $a['inserat_login'][$i], $a['headline_single_char3'][$i], $a['headline_single_char4'][$i]);
      foreach($values as $val) $lines .= str_replace($SeperatroValue,'',$val).$SeperatroValue;
      $lines .= "\n";
  }
  
  if($configurationForm=='POST'){
      //@chmod($configfile ,0700);
      if( file_put_contents($configfile,$lines) ===false )die('failed to write config');
      else echo "<p><span style=\"color:green\" id=\"settings_saved_ok\">saved</span></p><script language=\"javascript\">setTimeout(function(){ document.getElementById('settings_saved_ok').style.display='none' }, 5500);</script>";
  }else $configfileTextGET = $lines;
}

function sanizTitl($a){
  $a = str_replace('<small>','#&§small#&§',$a);
  $a = strip_tags($a);
  $a = str_replace('#&§small#&§','<small>',$a);
  return $a;
}

//translate
function l($text){
  global $translated, $lang;
  if( array_key_exists($text,$translated[$lang]))return $translated[$lang][$text];
  else if( array_key_exists($text,$translated[substr($lang,0,2)]) && $lang=='de')return $translated["de_DE"][$text];
  else return $text;  
}

//read config
if( file_exists($configfile ) || $configfileTextGET!='' ){
  if($configfileTextGET!=''){
    $lines = explode("\n",$configfileTextGET);
  }else{
	  //@chmod($configfile ,0400);
	  $handle = fopen($configfile, "r");
	  $lines = array();
	  if ($handle) {
		  while (($line = fgets($handle)) !== false) {
		  $lines[] = $line;
		  }
		}
    fclose($handle);
    //@chmod($configfile ,0000);
	}
	
	foreach($lines as $line){
		  /*
		   *if(substr($line,0,20)=="config3243443sda22323v 2asd23"){
		    $e = explode($SeperatroValue,str_replace('config3243443sda22323v 2asd23','',$line));
		    if (isset($e[0]) && $e[0]=='1') $headlineVerticalFixed = true;
		    if (isset($e[0]) && $e[0]=='1') $headlineVerticalFixed = true; else 
		   * 
		   */
      $e = explode($SeperatroValue,$line);
      if( count($e)<3 )continue;
      $name     = substr( sanizTitl($e[0]) ,0,60 );
      $headline = substr( sanizTitl($e[1]) ,0,60 );
      $url      = strip_tags($e[2]);
      if(substr($url,0,7)!='http://' && substr($url,0,8)!='https://' && substr($url,0,5)!='ftp://' && substr($url,0,6)!='ftps://'){
        if( strpos($url,'../')!==false ){
          echo '<p>Failed to process: '.strip_tags($url).'. "../" in the filename are allowed to open.';
          $url='';
        }
      
        if(!isset(pathinfo($url)['dirname']) || pathinfo($url)['dirname']=='' || pathinfo($url)['dirname']=='.'){}
        else if( pathinfo($url)['dirname'] != pathinfo( $_SERVER['SCRIPT_NAME'] )['dirname'] ){
          if($allow_open_uploaded_files===true){}
          else{
            echo '<p>Failed to process: '.strip_tags($url).'. Only files in this folder are allowed to open.';
            $url='';
          }
        }
      }
      $border   = (int)$e[3]; 
      if(preg_match('#^\#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$#',$e[4],$match)!==false) $color    = $e[4];
      $disable  = (isset($e[5]))? (int)$e[5]:'';
      $char_c   = (isset($e[6]))? substr($e[6],0,3) :'' ;
      if(preg_match('#^\#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$#',$e[7],$match)!==false) $bg_color = $e[7];
      if( trim($name)=='' && trim($url) == '')continue;
      while( isset($timetables[ $name ]) ) $name=$name.'_';
      $timetables[ $name ] = $url ;
      $headlinFix[ $name ] = $headline;
      $seperaLine[ $name ] = $border;
      $colors[ $name ]     = $color;
      $disabled[ $name ]   = $disable;
      $char_cell[ $name ]  = $char_c;
      $bg_colors[ $name ]  = trim($bg_color);
      $headline_single_char[ $name ]  = substr($e[8],0,1);
      $headline_single_char2[ $name ] = substr($e[9],0,1);
      $inserat_link[ $name ] = isset($e[10])?str_replace('javascript:','',strip_tags($e[10])):'';
      $inserat_login[ $name ] = isset($e[11])?str_replace('javascript:','',strip_tags($e[11])):'';
      $headline_single_char3[ $name ] = substr($e[12],0,1);
      $headline_single_char4[ $name ] = trim(explode(' ',substr($e[13],0,5))[0]);
	}
}



?><html>
<head>
<title>Calendar overview</title>
<?php if($noindex) echo '<meta name="robots" content="noindex">'; ?>
<meta name="referrer" content="none">
<?php echo $html_head_add; ?>
<style>
table{ border-collapse: collapse; }
.td_1_title {
  vertical-align: bottom;
  padding-bottom: 10pt;
}
.a50pz_transp{ opacity: 0.5; }
.c{  display:inline-block;  background: #ececec;}
.c_0_75{
  width: 0px;
  height: 0px;
  -webkit-transform:rotate(360deg);
  border-style: solid;
  border-width: 0 0 25px 25px;
  border-color: transparent transparent #35b19b transparent;
}
.c_0_25{
  width: 0px;
  height: 0px;
  -webkit-transform:rotate(360deg);
  border-style: solid;
  border-width: 25px 25px 0 0;
  border-color: #35b19b transparent transparent transparent;
}
.c_2{
  width: 0px;
  height: 0px;
  -webkit-transform:rotate(360deg);
  border-style: solid;
  border-width: 12.5px 25px 12.5px 0;
  border-color: transparent #35b19b transparent transparent;
}
.c_1{  background: #35b19b; }
.c_, .c_1{ 
  display:inline-block;
  width: 25px;
  height: 25px;
}

tr td {
  }
tr td:nth-last-child(1) {
  width:20pt;
}
#settings tr td:nth-last-child(1) {
  width:auto;
}
/*
tr:hover td {
  border-top: 3pt blue solid;
  border-bottom: 3pt blue solid;
}
thead tr:hover td, tr:nth-child(0):hover td, tr:hover td.month_text{
  border-top: 0px white none;
  border-bottom: 0px white none;
}*/
thead tr td{   vertical-align:top; }
.weekend{  background:#dddddd; }
.weekend + .weekend { border-bottom: solid 1pt black; }
.weekend_sa { background:#ececb7; }
.month_text{   background:white;  color:#555555; }
.vertical{  writing-mode:tb-rl; writing-mode:vertical-rl; }
.vertical_fixed{  top:12pt; background:white; padding-top:5pt; padding:2pt; z-index:3;}
.headline_scrollable{  /*opacity: 0.15;*/ /*display: none; */ }
.headline_scrollable:hover{  opacity: 1.00; /*display: none; */ }
.headline_single_char_fixed, .headline_single_char_fixed2 { position:fixed; top:0px; width:17pt; z-index:3; padding-left:2pt; background: white;}
.headline_single_char_fixed a span { font-family: DejaVu Sans Light; font-size: 6.5pt; margin-left: -1pt; }
.headline_single_char_scrollable{  opacity: 0.35; /*display: none; */ }
.headline_single_char_scrollable:hover{  opacity: 1.00; /*display: none; */ }
.headline_single_char_hide{  display: none; }
.hide_at_fixed{ display:none }
.hide_at_scrollable{ display:none }
.failed_load_data{  background:white;}
.char_cell{  position: absolute;z-index: 1;opacity:0.5; padding-left:5pt;}
.char_cell__lower{ padding-top: 4pt;}
.vertical.headline_scrollable img {    display: none;}
.vertical_fixed .name_hide_firstpart {    display: none;}

.visibility_none{ visibility:hidden;}
@media print{
  .headline_scrollable, .headline_single_char_scrollable{ display: auto; opacity:1.0;}
  .vertical_fixed{ display:none; }
  .c_1{    border: 2px #35b19b solid;   }
  .weekend td:nth-child(1){    border: 2px #cccccc solid;  }
  .weekend td{    border-bottom: 2px #cccccc solid;  }
  #scroll_choose{    display:none;  }
  hide_at_scrollable{display:block;}
}

@media screen and (max-width: 480px){
  #scroll_choose{    display:none;  }
}
.event_title{ display:none; }
<?php
    foreach($bg_colors as $c){
        $c_int = str_replace('#','',$c);
        echo ".bg_color_".$c_int."{ background: $c; }\n";
    }
    
   foreach($colors as $c){
        $c_int = str_replace('#','',$c);
        echo ".color_".$c_int."_025{ border-color: $c transparent  transparent transparent; }\n";
        echo ".color_".$c_int."_075{ border-color: transparent  transparent $c transparent; }\n";
        echo ".color_".$c_int."_200{ border-color: transparent $c transparent transparent; }\n";
        echo ".color_".$c_int."_100{ background: $c; }\n";
    }

    foreach($seperaLine as $s){
     if( isset($s) && $s!='' ){  
        echo ".sep_line_".(int)$s."{ border-right: ".((int)$s)."pt black solid; }";
      }
    }
?>
</style>
<script language="javascript">
function outTd(elem){
  document.getElementById('highlight_line1').style.display='none';document.getElementById('highlight_line2').style.display='none';
  document.getElementById('highlight_line3').style.display='none';document.getElementById('highlight_line4').style.display='none';
  };
function overTd(elem){
  var rect=elem.parentNode.getBoundingClientRect();//tr
  var l1=document.getElementById('highlight_line1');l1.style.display='block';
  l1.style.top=rect.top + window.scrollY;l1.width=rect.left-rect.right;l1.style.width=(rect.right-rect.left);
  var l2=document.getElementById('highlight_line2');l2.style.display='block';
  l2.style.top=rect.bottom + window.scrollY;l2.style.width=(rect.right-rect.left);
  
  rect=elem.getBoundingClientRect(); //td
  var l3=document.getElementById('highlight_line3');
  l3.style.display='block';l3.style.left=rect.left - rect.width + window.scrollX;
  l3.style.top=0; l3.style.height=(rect.bottom)+window.scrollY;
  var l4=document.getElementById('highlight_line4');
  l4.style.display='block';l4.style.left=rect.left + window.scrollX;
  l4.style.top=0; l4.style.height=(rect.bottom)+window.scrollY;
}


//make it past able to office-programms
window.addEventListener('copy', (event) => {

  /*
  var x = document.querySelectorAll('div[class=\'char_cell\']')
  var i;
  for (i = 0; i < x.length; i++) {
    x[i].parentNode.firstChild.innerHTML="";
  }
  */

  var x = document.querySelectorAll('div[class*=\'_075\']')
  var i;
  for (i = 0; i < x.length; i++) {
    x[i].parentNode.firstChild.innerHTML=x[i].parentNode.firstChild.innerText+"x";
  } 

  var x = document.querySelectorAll('div[class*=\'_100\']')
  var i;
  for (i = 0; i < x.length; i++) {
    x[i].parentNode.firstChild.innerHTML=x[i].parentNode.firstChild.innerText+"x";
  } 

});

</script>
</head>
<body>
<?php

if($allow_open_uploaded_files) echo "<p>from<br>your just<br>uploaded<br>files</p>";
if( isset($_GET['name']) && !isset($_GET['just_an_example']) && !isset($_GET['notipp'])){
  $tip = "Save this Internet-Adress as favorit / bookmark (the long URL with all the parameter) .<br>Than all the configuration-fields are already filled in again, if you visit the bookmark again.";
  echo "<p ><fieldset id=\"fields\" style=\"margin-left:200pt;z-index:11;background:#ffffffdd;position:relative\"><legend for=\"fields\">Tip:</legend>".l($tip)."</fieldset></p>";  
}

$f = 'calendar_icons/';
if( file_exists($f) ){
  foreach($timetables as $name=>$timetableUrl){ 
    if( preg_match('#https?://(www.)?([^/.]*\.)?([^/\.]*)\.([^/\.]*)/#',$timetableUrl,$match)!==false && count($match)>2){
      $a = $match[ count($match)-2 ]; $b = $match[ count($match)-1 ];
      $ico = $a.'.'.$b.'.png';
      if( strlen($ico) > 5 && file_exists($f.$ico) ) $icons[$name]=$f.$ico;
      $ico = $a.'.png';
      if( strlen($ico) > 5 && file_exists($f.$ico) ) $icons[$name]=$f.$ico;
    }
    if( preg_match('#https?://(www.)?[^/]*/extract_calendar.php/[0-9-]?/?[0-9-]?/?[0-9-]?/?(https?://)?(www.)?([^/.]*\.)?([^/\.]*)\.([^/\.]*)/#',$timetableUrl,$match)!==false && count($match)>2){
      $a = $match[ count($match)-2 ]; $b = $match[ count($match)-1 ];
      $ico = $a.'.'.$b.'.png';
      if( strlen($ico) > 5 && file_exists($f.$ico) ) $icons[$name]=$f.$ico;
      $ico = $a.'.png';
      if( strlen($ico) > 5 && file_exists($f.$ico) ) $icons[$name]=$f.$ico;
    }
  }
}

$timestamp = time();
$datum = date("d.m.Y - H:i", $timestamp);
//echo "<small>Erstellt am ".$datum."</small><br>";

//config GUI (=settings)
if(!$allow_open_uploaded_files){ $s1='';$s2='';}
else{ $s1='<s title="is disabled in file-upload-mode">'; $s2='</s>'; }
echo "<p style=\"float:right;text-align:right;font-size:8pt;position:fixed;right:4pt;\">";
echo "<span><label for=\"show_event_title\"><span style=\"cursor:help;border-bottom:dotted 1px black;\" title=\"some Calendar have summary of the events: e.g. `booking of Mr. X` or other notes;MouseOver the Text in the Table, gives full text & Day-end of Event\">".l('show notices')."</span></label><input type=\"checkbox\" onclick=\"var e=document.getElementsByClassName('event_title');for(i=0;i<e.length;i++){ if(this.checked){e[i].style.display='block';}else{e[i].style.display='none';} } \" id=\"show_event_title\" /><br></span>";
echo "<a href=\"#\" onClick=\"if(document.getElementById('settings_content').style.display!='none')document.getElementById('settings_content').style.display='none'; else document.getElementById('settings_content').style.display='block'\" style=\"margin-right:0pt;\">$s1 ".l("config: show/hide")." $s2 <span style=\"font-size:16pt;\">&#9881;</span> &nbsp; </a>";
if($allow_upload_form && $allow_open_uploaded_files)echo "<span><br>Back to normal Mode: <br><a href=\"?openconfig=1&just_an_example=1\">work with Online-iCal</a></span>";
echo "</p><span style=\"clear:both\"></span>";
if($allow_upload_form && $allow_open_uploaded_files)echo "<br><br>";
echo "<script>
function addExample(id){
  var example_file = 'ical_calendar.ics';
  var value = document.getElementById(id).value;
  if(value!='' && value!=example_file) if(!confirm('".l('replace the URL, with example calendar?')."'))return false;
  if(value==example_file){ alert('".l("already inserted")."');return false; }
  document.getElementById(id).value=example_file;return false;
}
</script>";

if(!$allow_open_uploaded_files){
  $timetables[0] = ''; //just placeholder for new Entry
  $hid=''; if( !isset($_POST['name']) && !isset($_GET['openconfig']) ) $hid = ' style="display:none" ';
  echo "<div $hid id=\"settings_content\"><form method=\"$configurationForm\"><br/><br/><br/><br/><br/>";
  if($allow_upload_form)echo "<p align=\"right\" style=\"float:right\"><a href=\"?uploaded_files_form=1\" title=\"you can upload files from you local PC (not mixing with online urls)\"><input type=\"button\" value=\"Change to Upload-Mode\"></a><!--<small><br>upload files from your computer</small>--></p>";
  echo "<br/><h2>".l("Configuration")."</h2> ";
  $i=0;
  echo "<script language=\"javascript\" type=\"text/javascript\">
  function set_move(elemId,direction){
    if(direction=='up')document.getElementById(elemId).parentNode.insertBefore(document.getElementById(elemId),document.getElementById(elemId).previousSibling);
    if(direction=='down')document.getElementById(elemId).parentNode.insertBefore(document.getElementById(elemId).nextSibling,document.getElementById(elemId));
    return false;
  }
  </script>";

  foreach($timetables as $name=>$timetableUrl){
      $i++;
      echo "<fieldset id=\"fields_cal_$i\"><legend for=\"fields_cal_$i\">".l("Calendar")." No.".l($i)."</legend>";
      echo "<div class=\"set_move\" style=\"text-align: right;float:righ;\"> position: <a href=\"#\" onclick=\"set_move('fields_cal_$i','up');return false;\">up</a> <a href=\"#\" onclick=\"set_move('fields_cal_$i','down');return false;\">down</a></div>";
      if($name=='') echo "<br><u>".l("Add new entry").":</u></br><br>";
      echo "<table border=\"0\" id=\"settings\">";
      $sel_active = ''; $sel_disabled = ''; $op_dis=''; $op_trans='';
      if((isset($disabled[$name])&&$disabled[$name]==1) ) $op_dis='selected';
      if((isset($disabled[$name])&&$disabled[$name]==2) ) $op_trans='selected'; 
      echo "<tr><td align=\"right\">&nbsp;</td>";
      echo "
      <select name=\"disable[$i]\" >
        <option value=''>activ</option>
        <option value='1' $op_dis>disabled</option>
        <option value='2' $op_trans>half_transparent</option>
      </select>
      </td></tr>";
      echo "<tr><td align=\"right\" valign=\"top\">name:&nbsp;</td>";
      echo "<td><input type=\"text\" maxlength=\"60\" name=\"name[$i]\" value=\"".((isset($name)&&$name!==0)?$name:'')."\"/></td></tr>";
      echo "<tr><td></td><td><small style=\"\"><span style=\"float:left; height:1em;\">details for name:&nbsp;</span><span style=\"display:block\">".l("First Name part hidden: depending on your settings: possible the first part of the name (all before \" \" or \"_\") will be hidden (sometimes).")."</span></small><br/></td></tr>";
      echo "<tr><td align=\"right\" valign=\"top\">url: </td>";
      echo "<td><input type=\"text\" name=\"url[$i]\" id=\"url[$i]\" size=64 value=\"".(isset($timetableUrl)?$timetableUrl:'')."\"/><br>
      ".l("Insert here your calendar Export-Adress/URL.")." (".l("e.g.")." .iCal/.ics);
      <nobr><em><a style=\"color:#555555;font-size:8pt;cursor:pointer\" href=\"#\" onClick=\"addExample('url[$i]')\" > ".l('add example test file.ics')." </a></em></nobr><br>
      <details><summary style=\"color:#555555;cursor:pointer;font-size:10pt;\">".l("Customer of")." tourist-online, e-domozil, traum-ferienw? (click)</summary>
      <small style=\"color:#555555;\"><span style=\"float:left; height:5em;\"></span><span style=\"display:block\">
      There is <b>experimental</b> support of:   tourist-online.de; e-domizil.de (for all languages: .de|.at|.ch|.com|.es|.fr or edom.pl); ferienhausmarkt.com, pensionen-weltweit.de, ostsee-strandurlaub.net; please use <a href=\"http://www.multiple-ical.de/extract_calendar.php\" target=\"_blank\">this Convert Service</a>  (if Webservice offline, than can install it on your own webserver: <a href=\"https://gitlab.com/soerenj/overview_multiple_ical_calendar_bookings\" target=\"_blank\">SourceCode</a>)
      </small>
      </details>
      </td></tr>";
      echo "<tr><td></td><td>";
      echo "<br><br><div style=\"padding-left:0pt\">";
      echo "<details><summary style=\"cursor:pointer;color:#555555;\"><small>".l("more settings (unimportant)")." (click):</small></summary>";
	  echo "<br>inserat url (just for link; unimportant):";
      echo "<input type=\"text\" name=\"inserat_link[$i]\" id=\"inserat_link[$i]\" size=64 value=\"".(isset($inserat_link[ $name ])?$inserat_link[ $name ]:'')."\"/>";
      echo "<br>inserat login url (just for link; unimportant):";
      echo "<input type=\"text\" name=\"inserat_login[$i]\" id=\"inserat_login[$i]\" size=64 value=\"".(isset($inserat_login[ $name ])?$inserat_login[ $name ]:'')."\"/>";
      if($headlineVerticalFixed){ $s='hidden'; }else { $s='text';}
      if(!$headlineVerticalFixed)echo "headline fixed: ";
      echo "<input type=\"$s\" maxlength=\"30\" name=\"headline[$i]\" value=\"".(isset($headlinFix[$name])?$headlinFix[ $name ]:'')."\"/>";
      if($headlineVerticalFixed)echo"<br/>";
      echo l("Cell character").":           <input type=\"text\" maxlength=\"2\" size=\"2\" name=\"char_cell[$i]\" value=\"".((isset($char_cell[$name])&&$char_cell[$name]!=='')?$char_cell[$name]:'')."\"/><br/>";
      echo l("Single character Headline").":  <input type=\"text\" name=\"headline_single_char[$i]\" value=\"". $headline_single_char[$name]."\" size=\"1\"/><br/>";
      echo l("Single character Headline")."2:  <input type=\"text\" name=\"headline_single_char2[$i]\" value=\"". $headline_single_char2[$name]."\" size=\"1\"/><br/>";
      echo l("Single character Headline")."3:  <input type=\"text\" name=\"headline_single_char3[$i]\" value=\"". $headline_single_char3[$name]."\" size=\"1\"/> <br/>";
      echo l("a few characters Headline")."4:  <input type=\"text\" name=\"headline_single_char4[$i]\" value=\"". $headline_single_char4[$name]."\" size=\"5\"/> <span style=\"font-size:8pt;\">max 3 characters (very small)<br/>";
      echo "<small style=\"color:#555555;padding-left:2pt\">will be shown on each table-cell e.g.: o/u og/ug up/down (or @ (( @ will be replaced with=>&#127968; (on SingleChrakterHeadline3: replaced: a=>&#128259;))) ) ; single (or two) charakter/symbol; or utf8</small><br/><br/>";
      echo l("border width (right)").": <input type=\"number\" max=20 name=\"seperaLine[$i]\" value=\"".(isset($seperaLine[$name])?$seperaLine[ $name ]:'')."\"/><br/>";
      echo l("color").":  <input type=\"color\" name=\"color[$i]\" value=\"".((!isset($colors[$name]) || $colors[$name]=='')?'#35b19b':$colors[ $name ])."\"/> &nbsp;  &nbsp;  &nbsp;  &nbsp; ";
      echo l("Background-Color").":  <input type=\"color\" name=\"bg_color[$i]\" value=\"".((!isset($bg_colors[$name]) || $bg_colors[$name]=='')?'#ececec':$bg_colors[ $name ])."\"/><br/>";
      echo "<small>".l("...to delete an entry: just delete the text in <i>name</i> and <i>url</i>")."</small>";
      echo "</details></div>";
      echo "</td></tr>";
      echo "</table>";
      echo "</fieldset><br><br>";
      echo "<hr>";
  }
  unset($timetables[0]); //just placeholder for new Entry
  if($showFiveYearsInFuturePreset==false){
    if($showFiveYearsInFuture===false) $checked = ''; else $checked = ' checked="checked" '; 
    echo l("Show more than 2Years?: 5 Years: ")."<input type=\"checkbox\" name=\"showFiveYearsInFuture\" value=\"1\" id=\"showFiveYearsInFuture_checkbox\" $checked  \>";
    if($configurationForm=="POST")echo" <em>(will be only temporary set)</em>";
    echo"<hr>";
	}    
  echo "<input type=\"submit\" name=\"submit\"/><br/><br/><br/><br/>";
  echo "</form></div>";

}

//process normal calendar-data (write in Calendar-Array)
foreach($timetables as $timetableName => $url){
  $n = $timetableName;  if($url=='')continue;
  if( isset( $disabled[ $timetableName ] ) && $disabled[ $timetableName ]==1 ) unset($timetables[$timetableName]);
  $timetable=getDatesFromIcs($url);
  if($timetable===false)continue;
  if( count($timetable)==0 ) continue;
  $abs_minDate = (new DateTime())->modify('-90 days');
  foreach($timetable as $key => $t){
    if($t=='') continue;
    //if($key=="manuell") continue;
    $start = new DateTime($t['start']);
    $end   = new DateTime($t['end']);
    $date  = $start;
    if($start>=$end)continue;//in case of broken ical
    if($start<=$abs_minDate && $end<=$abs_minDate)continue;//ignore old dates
    //echo $date->format('Y-m-d').'<br>';
    $y = $date->format('Y');    $m = $date->format('n');    $d = $date->format('j');
    if(isset($t['title'])&&$t['title']!='')$title_calendar[$y][$m][$d][$n]=trim(strip_tags($t['title']));
    if( isset($calendar[$y][$m][$d][$n]) && $calendar[$y][$m][$d][$n]!='' ){    //check if day is already a departure day and merge
      if($calendar[$y][$m][$d][$n]=="0.25") $calendar[$y][$m][$d][$n] = 2;
      else if($calendar[$y][$m][$d][$n]=="1") $calendar[$y][$m][$d][$n] = 1;
      else if($calendar[$y][$m][$d][$n]=="0.75") $calendar[$y][$m][$d][$n] = 0.75;
      //else{ echo "<p>ProgrammError: at date ".$date->format('Y-m-d')." ".$calendar[$y][$m][$d][$n]."</p>"; var_dump($calendar[$y][$m][$d][$n]);  }
    } 
    else $calendar[$y][$m][$d][$n] = 0.75;
	if( isset($t['unclear']) ){ //could be just a note on airbnb ?!
	  $calendar[$y][$m][$d][$n] = -1;
	}
    $max=356*5;$i=0; //prevent infinit loop
    do{
      $date->modify('+1 day');
      $y = $date->format('Y');    $m = $date->format('n');    $d = $date->format('j');
      /* overwrite whatever comes
      //if( isset($calendar[$y][$m][$d][$n]) ) echo "<p>Programmfehler: beim Datum ".$date->format('Y-m-d')."</p>";
      if( isset($calendar[$y][$m][$d][$n]) ) {
        if( $calendar[$y][$m][$d][$n]=="0.25") $calendar[$y][$m][$d][$n] = 1;
        else if($calendar[$y][$m][$d][$n]=="1") $calendar[$y][$m][$d][$n] = 1;
        else if($calendar[$y][$m][$d][$n]=="0.75") $calendar[$y][$m][$d][$n] = 1;
        else{ echo "<p>ProgrammError: at date ".$date->format('Y-m-d')." ".$calendar[$y][$m][$d][$n]."</p>"; var_dump($calendar[$y][$m][$d][$n]);  }
      }else */
      $calendar[$y][$m][$d][$n] = 1;
      //echo $date->format('Y-m-d').'<br>';
      $i++;
      //if($i==$max) echo "<p>Warning: Event longer then ".$max."days was stoped after $max days. In ".strip_tags($timetableName).'<br><small>(start:'.$start->format('Y-n-j').'_end:'.$end->format('Y-n-j').": Event is longer 356days or more (is this an error or correct?)</small> </p>";
    } while ( $date != $end && $date<=$end && $i<$max);
    if($i<$max)$calendar[$y][$m][$d][$n] = 0.25; //departure-day
  }
}



$date = new DateTime( 'now');
$y = $date->format('Y');
$m = $date->format('n');



echo "
<div id=\"scroll_choose\" style=\"position:fixed;right:0px;margin-top:40pt;\">
".l("month").":<small><a href=\"#\" onclick=\"if(document.getElementById('scroll_choose_content').style.display!='none')document.getElementById('scroll_choose_content').style.display='none'; else document.getElementById('scroll_choose_content').style.display='';return false;\" title=\"hide this menue-list\">".l("hide")."</a></small>
<div id=\"scroll_choose_content\">";
$showed_month_count=1;
$max_showed_month_count=16;
if($showFiveYearsInFuture===true)$max_showed_month_count=12*5;
while($showed_month_count<$max_showed_month_count){
  for(;$m<=12 && $showed_month_count<=$max_showed_month_count;$m++){ 
    echo "<a href=\"#".$y."_".$m."\">".$y."_".$m."</a><br>"; $showed_month_count++;
  }
  $y++;$m=1;
}
echo "
</div></div>
";

$date = new DateTime($y);
$y=$date->format('Y');
$m=$date->format('n');




//normaly not needed: (source: https://www.lima-city.de/thread/fatal-error-call-to-undefined-function-cal_days_in_month)
if(!function_exists("cal_days_in_month"))
{
if(!defined('CAL_GREGORIAN'))define('CAL_GREGORIAN','');
 function cal_days_in_month($calendar, $month, $year)
 {
   /*
 $newtag = 28;
 for($i = 28; $i <= 31; $i++)
 {
  $timestmp = mktime(0,0,0, $month, $i, $year);
  $newtag = intval(date("j", $timestmp));
  if($newtag < $i) break;
 }
 return $i-1;*/
 #source: http://php.net/manual/en/function.cal-days-in-month.php#38666
 return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31); 
 }
}

if( !isset($title_calendar) || count($title_calendar)==0 )echo "<script language=\"javascript\">document.getElementById('show_event_title').parentNode.style.display='none';</script>";
else echo "<script language=\"javascript\">if(document.getElementById('show_event_title').checked==true)document.getElementById('show_event_title').click();</script>";

//clean up, for disabled Icals
foreach ($timetables as $name => $dataWaste){
  if( isset( $disabled[ $name ] ) && $disabled[ $name ]==1 ) unset($timetables[$name]);
}

$headline_single_char_array = array($headline_single_char,$headline_single_char2,$headline_single_char3,$headline_single_char4); 
$showed_month_count=1;
$max_showed_month_count=16;
if($showFiveYearsInFuture===true)$max_showed_month_count=12*5;
$v_fixed_only_first=$headlineVerticalFixed;
$showFixedHeadlines=false; //init
while($showed_month_count<$max_showed_month_count){
  for(;$m<=12&& $showed_month_count<=$max_showed_month_count;$m++){
    $showed_month_count++;
    if($showed_month_count==3)$showFixedHeadlines=true;//not the first one (because left margin, whereever there comes from)
    //if( !isset($calendar[$y]) || count($calendar[$y])==0 ){echo "no Entrys for $y "; break;}
    echo "<a name=\"".$y."_".$m."\"></a><div class=\"cal_table\" id=\"".$y."_".$m."\" style=\"\">";
    drawTable($y,$m, $timetables, $calendar,$colors,$bg_colors,$seperaLine,$headlinFix,$char_cell,$icons,$headline_single_char_array,$inserat_link,$inserat_login,$v_fixed_only_first,$title_calendar,$disabled,$showFixedHeadlines);
    echo "</div>";
	$showFixedHeadlines=false;//only once
    if(!$headlineVerticalFixed)$v_fixed_only_first = false;
  }
  $y++;$m=1;
}


function get__my_locale(){
  //set language of Week/Month-Day-Names
  $l='';
  if(class_exists('Locale'))$l = Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']);
  else{
    $eArray = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
    foreach($eArray as $e){
        if(strlen($e)==2 && 'de'==strtolower($e)){ $l="de-DE"; break;}
        if(strlen($e)==5){ $l=$e; break;}
    }
    $l_ex = explode('-',$l);
    if(isset($l_ex[1]))$l = strtolower($l_ex[0]).'-'.strtoupper($l_ex[1]);
    $l = str_replace('-','_',$l);
  }
  return $l;
}

//var_dump($calendar);
function drawTable($y,$m, $timetables, $calendar,$colors,$bg_colors,$seperaLine,$headlinFix,$char_cell,$icons,$headline_single_char_array,$inserat_link,$inserat_login,$headlineVerticalFixed=false,$title_calendar=array(),$disabled,$showFixedHeadlines){
$headline_single_char = $headline_single_char_array[0];
$headline_single_char2 = $headline_single_char_array[1];
$headline_single_char3 = $headline_single_char_array[2];
$headline_single_char4 = $headline_single_char_array[3];
  //$y=2018;
  //$m=11;
  $days_in_m=cal_days_in_month( CAL_GREGORIAN , $m , $y );
  $date = new DateTime($y.'-'.$m.'-01');
  $number_day_in_week=$date->format('N');

  echo "<div id=\"highlight_line1\" style=\"z-index:10;display:none;position:absolute;margin-left:20pt;width:200px;height:3pt;background:blue;opacity:0.8\"></div>"; //Hight current line on mouse over
  echo "<div id=\"highlight_line2\" style=\"z-index:10;display:none;position:absolute;margin-left:20pt;width:200px;height:3pt;background:blue;opacity:0.8\"></div>";
  echo "<div id=\"highlight_line3\" style=\"z-index:10;display:none;position:absolute;margin-left:20pt;height:200px;width:3pt;background:blue;opacity:0.8\"></div>"; //Hight current line on mouse over
  echo "<div id=\"highlight_line4\" style=\"z-index:10;display:none;position:absolute;margin-left:20pt;height:200px;width:3pt;background:blue;opacity:0.8\"></div>";
  
  echo "<table>";
  echo "<thead>";
  
  
  if($headlineVerticalFixed===false){
    echo "<tr>";
    echo "<td></td>";
    echo "<td></td>";
    foreach ($timetables as $name => $dataWaste){
      echo "<td style=\"background:#ffffff;\">";
      if( isset($headlinFix[$name]) ) echo "<div style=\"position:fixed;height: 14pt;min-height: 14pt;background:#ffffff;z-index:2;\">".$headlinFix[$name]."</div>"; 
      echo "</td>";
    }
    echo "</tr>";
  }
  
  
  $l=get__my_locale();
  if($l!='' && strlen($l)==5){
	  if(!setLocale(LC_TIME,trim($l).".UTF-8")) if(!setLocale(LC_TIME,trim($l).".UTF8")) setLocale(LC_TIME,trim($l));
  }
  
  $out = '';
  $out.= "<tr>";
  $out.= "<td></td>";
  $out.= "<td class=\"td_1_title\"><b>";
  $date = new DateTime($y.'-'.$m.'-1'); 
  $out.="<b>";
  $out.=strftime("%B",$date->format('U'));
  $out.="</b>";
  $out.=strftime("%Y-%m",$date->format('U'));
  $out.="</td>";
  echo "</b></td>";
foreach ($timetables as $name => $dataWaste){
	$i++;
	//$icons[$name]="/icons/a.gif";
	
    $headline_single_char[$name]  = str_replace('@','&#127968;',$headline_single_char[$name]);
    $headline_single_char2[$name] = str_replace('@','&#127968;',$headline_single_char2[$name]);
    $headline_single_char3[$name] = str_replace('@','&#127968;',$headline_single_char3[$name]);
    $headline_single_char4[$name] = str_replace('@','&#127968;',$headline_single_char4[$name]);
    $headline_single_char3[$name] = str_replace('a','<span title="possible meaning??: in auto sync?">&#128259;</span>',$headline_single_char3[$name]);
    if( isset($seperaLine[$name]) && $seperaLine[$name]!='' ) $class_td = "class=\" sep_line_".(int)$seperaLine[$name]."\" "; else $class_td ='';
	//if($i==1)$styleAdd=' '; else  $styleAdd='';
	if(!$showFixedHeadlines)$styleAdd=' style=";visibility:hidden;"';else $styleAdd='';
    $out.= "<td $class_td $styleAdd>";
    
	
	$out.="<div class=\"headline_single_char_fixed\">";
    
        //if( isset($headlinFix[$name]) ) echo "<div style=\"position:fixed;background:#ffffffdd;\">".$headlinFix[$name]."</div>"; 
     	$s=''; $text="A"; 
        if( isset($headline_single_char[$name]) && $headline_single_char[$name]!='' ){
			$text=$headline_single_char[$name];
			if(strlen($headline_single_char[$name])==1)$s="padding-left:5pt;";
		}else $s=";visibility:hidden";
    	foreach($headline_single_char as $tmp){if($tmp!='')$out.="<br>";break;}
	
    	$t=12;
    	if($inserat_link[$name]!='')$openInseratTitleLink='open inserat '; else  $openInseratTitleLink='';
    	//$out.="<span style=\"top:".$t."pt;cursor:pointer;font-size:6pt;\" class=\"headline_single_char_fixed\" >";
        
        if($inserat_link[$name]!='')$out.="<a href=\"".str_replace("'","`",str_replace('"','`',$inserat_link[$name]))."\" target=\"_blank\" style=\"font-size:8pt\" rel=\"noopener noreferrer\" style=\"font-size:8pt\" class=\"hide_at_scrollable\"/>";
            //$out.="</span>";
        	//else 
        	if($inserat_link[$name]!=''){
        		$out.= "<span class=\"headline_single_char hide_at_scrollable\" style=\"font-size:5pt\">";
        		if(count($icons)>0 )$out.="<br>";
        		$out.= "<span title=\"open Inserat\">open</span>";
        		$out.= "</span>";
        	}else foreach($inserat_link as $tmp){if($tmp!='' && count($icons)>0)$out.="<br style=\"hide_at_scrollable\" />";break;}
        	
            if( isset($icons[$name]) ){ $out.= "<img style=\"z-index: 10;position: initial;top:".$t."pt;\" class=\"\" src=\"".$icons[$name]."\" rel=\"noreferrer\" onClick=\"javascript:alert('".str_replace("'","`",str_replace('"','`',$name.' \n'.str_replace('/','_',$inserat_link[$name])))."\nwill be open in new window, after ok')\"  title=\"$openInseratTitleLink".str_replace('"','`',$name)."\" />"; $t+=17;}
        if($inserat_link[$name]!='') $out.="</a>";
        
    	if($inserat_login[$name]!=''){
    	    //if( $inserat_link[$name] !='' || isset($icons[$name]) )$t+=12;
    	    $out.="<br><a style=\"top:".$t."pt;cursor:pointer;font-size:6pt;\" class=\"hide_at_scrollable\" href=\"".str_replace("'","`",str_replace('"','`',$inserat_login[$name]))."\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"font-size:8pt\"/>";
    	    $out.="<span title=\"open Login-Page of Inserat\">login</span>";
    	    $out.="</a>";
    		//$t+=12;
    	}//echo $out.=$name;//else foreach($inserat_login as $tmp)if($tmp!='')$out.="<br><span style=\"font-size:6pt;\">&nbsp;</span>";
	
    
        foreach($headline_single_char2 as $tmp)if($tmp!='')$has__headline_single_char2=true;
        if($has__headline_single_char2){
            $out.="<br><span style=\"padding-left:5pt\" class=\"hide_at_scrollable\">";
            if(isset($headline_single_char2[$name]) ){ $out.= $headline_single_char2[$name]; $t+=15;}
            $out.="</span>";
        }
        foreach($headline_single_char3 as $tmp)if($tmp!='')$has__headline_single_char3=true;
        if($has__headline_single_char3){
            $out.="<br><span style=\"padding-left:5pt\" class=\"hide_at_scrollable\">";
            if(isset($headline_single_char3[$name]) ){ $out.= $headline_single_char3[$name]; $t+=15;}
            $out.="</span>";
        }
        foreach($headline_single_char4 as $tmp)if($tmp!='')$has__headline_single_char4=true;
        if($has__headline_single_char4){
            if(isset($headline_single_char4[$name])&&$headline_single_char4[$name]!='')$bol=true; else $bol = false;
            if($bol)$st_add=';border-right: 1px solid #e6dfdf;'; else $st_add='';
            $out.="<br><span style=\"font-size: 13px;padding-left: 0px;$st_add\" class=\"hide_at_scrollable\">";
            if($bol){ $out.= $headline_single_char4[$name]; $t+=15;}
            $out.="</span>";
        }
     	if( !isset($icons[$name]) || explode(' ',$name)[1]=='') $classadd=" hide_at_fixed"; else $classadd ='';
        $out.= "<div class=\"vertical $classadd\" style=\"top:".$t."pt;\" title=\"".str_replace('"','`',$name)."\" >";
        $e = explode(" ",$name);
        if( count($e)<2 )$e = explode('_',$name);
        //if( !isset($e[1]) )
        if( isset($icons[$name]) ) $c = "name_hide_firstpart"; else $c="";
        $out.= "<span class=\"$c\" style=\"color:black\">".$e[0]."</span>";
        if( count($e)>1 && strlen($e[1])>0){
          unset($e[0]); $out.=' '.implode(' ',$e);
        } 
        //$out.= $name;
        $out.= "</div>";
	$out.="</div>";

    $out.= "</td>";
  }
  $out. "</tr>";
  if($headlineVerticalFixed===true){
    $o = str_replace('vertical','vertical vertical_fixed',$out);
    $o = str_replace('td_1_title','td_1_title visibility_none',$o);
    $o = str_replace('hide_at_scrollable','',$o);
    echo $o;
  }
  $a = str_replace('hide_at_fixed','',$out);
  $a = str_replace('vertical','vertical headline_scrollable', $a);
  $a = str_replace('headline_single_char_fixed2','headline_single_char_hide',$a);
  $a = str_replace('headline_single_char_fixed','headline_single_char_scrollable',$a);
  echo $a;
  
  
  
  echo "</thead>";
  echo "<tbody>";
  $was_set_month_text_vertical = false;
  $startday=1;
  if(date("Y")==$y && date('m')==$m )$startday=date('d'); //start with today
  for($d=$startday;$d<=$days_in_m ;$d++){
    $date = new DateTime($y.'-'.$m.'-'.$d);
    $class = '';
    if($number_day_in_week>5) $class='weekend';
    if($number_day_in_week==6) $class.=' weekend_sa';
    echo "<tr class=\"$class\">";
    if(($number_day_in_week==1) || ($d==1 && $number_day_in_week<5)) {
      $rowspan = " rowspan=\"".(8-$number_day_in_week)."\" ";
      if($days_in_m-$d<7) $rowspan = " rowspan=\"". ($days_in_m-$d+1)."\" ";
      if($days_in_m-$d==0) $rowspan = '';     
      $text = strftime("%Y-%b",$date->format('U'));
      if($d>28)$text = '';
      echo "<td $rowspan class=\"vertical month_text\" style=\"width:20pt\">$text</td>";
      $was_set_month_text_vertical = true;
    }
    if($was_set_month_text_vertical==false)  echo "<td style=\"background:white\" ></td>";
    $v = '';
    $s = ''; if($date->format('Y-m-d')==(new DateTime())->format('Y-m-d')) $s='text-shadow: 1px 1px black;';
    echo "<td style=\"width:70pt;$s\">";

    echo "".$d.": ";
    echo strftime("%a",$date->format('U'));
    echo "</td>";
    foreach ($timetables as $name => $dataWaste){
      $v = '';
      if( isset($calendar[$y][$m][$d][$name]) ) $v = $calendar[$y][$m][$d][$name];
      $class = "c_".str_replace('.','_',$v);
      //echo "".$name.":".$v."";
      $class_td  = '';
      if( isset($seperaLine[$name]) && $seperaLine[$name]!='' ){  
        $class_td .= " sep_line_".(int)$seperaLine[$name]." ";
      }
      $name_withAdded = $name;
      if( $v!=0 ){
        if($v===0.25 ) $name_withAdded.=' (depature)';
        if($v===0.75 ) $name_withAdded.=' occupied (arrival)';
        if( $v===2 )   $name_withAdded.=' occupied (arrival+depature)';
        if( $v===1 )   $name_withAdded.=' occupied';
		if( $v<0 )     $name_withAdded.=' error or note'; //e.g.-1 unclear
      }else $name_withAdded.=' free';
     // var_dump($title_calendar);die();
      if(isset($title_calendar[$y][$m][$d][$name])){
        $t=explode('__until_',$title_calendar[$y][$m][$d][$name])[0];
        $infoAdd=' <i class="event_title" style="position:absolute;z-index:22;opacity:0.75" title="'.$name.': '.$title_calendar[$y][$m][$d][$name].'">'.substr($t,0,8).'</i>';
		$name_withAdded .= ' '.$title_calendar[$y][$m][$d][$name];
      }else $infoAdd='';
      
      if($disabled[$name]===2)$class_td.=' a50pz_transp';
      echo "<td class=\"".$class_td."\" title=\"$name_withAdded\" onMouseOut=\"javascript:outTd(this);\" onMouseOver=\"javascript:overTd(this)\" >";
      echo $infoAdd;
      if($bg_colors[$name]!='')$class .= " bg_color_".str_replace('#','',$bg_colors[$name])." ";
      if( $v!=0 && isset($colors[$name]) ){
        $c = str_replace('#','',$colors[$name]);
        if($v===0.25 ) $class.= " color_".$c."_025 ";
        if($v===0.75 ) $class.= " color_".$c."_075 ";
        if( $v===2 )   $class.= " color_".$c."_200 ";
        if( $v===1 )   $class.= " color_".$c."_100 ";
      }
      if( isset($char_cell[$name]) && $char_cell[$name]!=''){
        if(strpos($char_cell[$name],'@')!==false) $char_cell[$name] = str_replace('@','&#127968;',$char_cell[$name]);
		if( $v<0 )$char_cell[$name].='<b>?</b>';//e.g. -1 unclear
        //if($char_cell[$name]=='u') $char_cell[$name] = '<i>'.$char_cell[$name].'</i>';
        echo "<div class=\"char_cell\" >".$char_cell[$name]."</div>";
      }
      if( $timetables[$name] === false ) $class.=' failed_load_data';
      echo "<div class=\"c $class\" ></div> </td>";
    }
    echo "<td></td>";
    echo "</tr>";
    $number_day_in_week++; if($number_day_in_week>7)$number_day_in_week=1;
  }
  echo "</tbody>";
  echo "</table>";
}







function download2($file){
 //if( substr($file,0,22) == 'https://www.airbnb.de/' ){
    if(preg_match("#^[a-zA-Z/\?_\-=\.:0-9]*$#", $file, $treffer)===1){
      exec( " wget $file -O Tmpfile.ics" );
      $file = "Tmpfile.ics";
    }else echo "<p>Error: cant validate Calendar URL $file. Please check for version of this programm.</p>";
 // }
 return $file; 
}

function getDatesFromIcs($file){
  if($file=='')return;
  //$isAirBnb=false;//default
  $file_orginal = $file;
  if( substr($file,0,24) == 'https://www.dropbox.com/' && substr($file,-4) == 'dl=0' ) $file = str_replace('?dl=0','?dl=1',$file);
  if( substr($file,0,22) == 'https://www.airbnb.de/' ){ /*$isAirBnb=true;*/ $file=download2($file); }
  else if( @ ($ics = file_get_contents($file)) === false) $file=download2($file); //first try
  if($file == "Tmpfile.ics") $ics = file_get_contents($file); //second try
  if($file == "Tmpfile.ics") unlink("Tmpfile.ics");
  if( !isset($ics) || $ics=='') {   echo "<p>Fehler beim Laden von $file_orginal </p>"; return false; }


  $events = explode('BEGIN:VEVENT',$ics);
  $timetable = array();
  foreach($events as $e){
	$return = getStartEnd($e);
	if($return['start']=='') continue;
	$timetable[] = $return;
  }
  //if(count($timetable)==0)var_dump($timetable);
  return $timetable;
}

function getStartEnd($e){
  global $event_title_hideNotes;
  $start = '';
  $end   = '';
  $title = '';
  $lines = preg_split('[\n|\r|\r\n|\n\r]',$e);
  foreach($lines as $l){
    $l = trim($l);
    if(substr($l,0,19)=='DTSTART;VALUE=DATE:')  $start = substr(str_replace('-','',str_replace('DTSTART;VALUE=DATE:','',$l)),0,8);
    if(substr($l,0,8)=='DTSTART:')  $start = substr(str_replace('-','',str_replace('DTSTART:','',$l)),0,8);
	  if(substr($l,0,17)=='DTSTART;TZID=CET:')  $start = substr(str_replace('-','',str_replace('DTSTART;TZID=CET:','',$l)),0,8);

    if(substr($l,0,17)=='DTEND;VALUE=DATE:')    $end = substr(str_replace('-','',str_replace('DTEND;VALUE=DATE:','',$l)),0,8);
    if(substr($l,0,6)=='DTEND:')    $end = substr(str_replace('-','',str_replace('DTEND:','',$l)),0,8);
	  if(substr($l,0,15)=='DTEND;TZID=CET:')  $end = substr(str_replace('-','',str_replace('DTEND;TZID=CET:','',$l)),0,8);
	
	  if(substr($l,0,8)=='SUMMARY:'){
		  if( array_search(strtolower(trim(substr($l,8))),$event_title_hideNotes)===false)$title = substr($l,8);
    }
  }
  if($start == $end) return array('start'=> '', 'end' => '');
  if($title!='') $title.='__until_'.$end;
  return array('start'=> $start, 'end' => $end , 'title' => $title);
}




echo "<p id=\"footer\" style=\"opacity:0.5;font-size: 9pt;\"><small>generated by \"overview_multiple_ical_calendar_bookings\" Programm-licence: AGPL <a target=\"_blank\" rel=\"no-follow\" href=\"https://gitlab.com/soerenj/overview_multiple_ical_calendar_bookings\">SourceCode</a></small><sa href=\"#\" style=\"cursor:pointer;padding-left:50pt;color:#888888;float:right\" title=\"hide footer\" onClick=\"document.getElementById('footer').style.display='none'\">x</a></p>";