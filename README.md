[![Screenshot4a small preview (link to full version)](screenshots/screenshot4a_thumb.png)](screenshots/screenshot4a.png)

?possible no longer developed?

# Show multiple ical files  ( calendar's overview)
## => [test it now](http://multiple-ical.de) <=
online Webservice: [ http://multiple-ical.de ](http://multiple-ical.de) (online until 11-2021)


Can only handle full-days events. Give you an overview above mulitple ical Calendar's. (multiple ical/ics files)

typical use case:
 - compare occupation Calendar from multiple booking portals. It load calendar from multiple location (multiple booking-Systems).
You get an overview about all occupation, of every Booking-Portal.

Requirement:
 - You need a (public accessible) iCal-Adress (.ics) from the booking systems.
 - And of cource, you need a Web-Server/Homepage with PHP. This Webserver should support shell_exec (possible it also works also without). No Database needed.


Securtiy:
I would be good to protect this file (e.g. rename this file or passwort-protection via .htacess). Otherwise possible someone abuse/hack your website to attac someone else. An SourceCode review aspecially in security-topics are welcome.



It's quick and dirty written. Anyway, have fun with it (not beautiful SourceCode).



# german: Zeige mehrere ical (Kalender Übersicht)
Benutze diese Software jetzt: [http://multiple-ical.de]( http://multiple-ical.de) (als Online-Dienst; bis November 2021 online)

z.B. für Belegungskalender/Buchungen (es kann nur Tage-weise Events). Überblick über alle Buchungs-Kalendar.
Es kann von verschiedenen Adressen (z.B. Buchungs-System) die Kalendar laden.
Damit bekommt man eine übersicht. Nur als übersicht zum ansehen.
Man braucht die iCal-Adresse vom Buchungs-system (oft auch Kalendar Export genannt). Die meisten Buchungs-Portal haben sowas. Und natürlich braucht man einen Web-Server/Homepage mit PHP (mit shell_exec() ).

Nochmal die Funktion in anderen worten beschrieben:
Buchungsportale zusammenfassen, Kalendar zusammenführen von Buchungen, Belegungen, Belegunspläne. Für z.B. Ferienwohnungen, Ferienhäuse, Gästehäuse, Gäste-Wohnungen, FeWo. Es soll helfen bei der Synchronisation mehrerer Belegungspläne. Es bietet jedoch nur den Überblick, nicht mehr.
 
Am besten sollte man ein Passwort-Schutz einrichten ( .htaccess ). Mindestens sollte man die .php Datei (und config.txt) umbenennen. Ansonsten könnte es sein, das deine Webseite wird gehackt ( oder deine Websseite andere anzugreift).

-------------------------




![Screenshot4](screenshots/screenshot4a.png)

![Screenshot4](screenshots/screenshot4b.png)


![Screenshot](screenshots/screenshot.png)


![Screenshot2](screenshots/screenshot2.png)


![Screenshot Config](screenshots/screenshot_config2.png)


Lizenz: AGPL
